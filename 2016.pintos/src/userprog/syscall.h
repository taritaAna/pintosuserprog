#ifndef USERPROG_SYSCALL_H
#define USERPROG_SYSCALL_H

#include "lib/stdbool.h"
void syscall_init (void);
typedef int pid_t;

#endif /* userprog/syscall.h */
