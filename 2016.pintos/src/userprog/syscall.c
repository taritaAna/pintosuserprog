#include "userprog/syscall.h"
#include <stdio.h>
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "threads/vaddr.h"
#include "devices/shutdown.h"
#include "userprog/process.h"


static void syscall_handler (struct intr_frame *);
static void exit (int);
static pid_t exec (const char *);

void
syscall_init (void) 
{
  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");
}

static void
syscall_handler (struct intr_frame *f)
{
  uint32_t* esp = f->esp;
  //TODO use is_valid_ptr to validate each argument
  int syscall_no = ((int*)f->esp)[0];

  switch (syscall_no) {
  case SYS_HALT:/* Halt the operating system. */
	  shutdown_power_off();
      break;
  case SYS_EXIT:/* Terminate this process. */
	  exit(*(esp+1));
	  break;
  case SYS_EXEC:/* Start another process. */
	  f->eax = exec((char*)*(esp+1));
  	  break;
  case SYS_WAIT:/* Wait for a child process to die. */
	  f->eax = process_wait(*(esp+1));
  	  break;
  case SYS_CREATE:/* Create a file. */
  	  //TODO
  	  break;
  case SYS_REMOVE:/* Delete a file. */
  	  //TODO
  	  break;
  case SYS_OPEN:/* Open a file. */
  	  //TODO
  	  break;
  case SYS_FILESIZE:/* Obtain a file's size. */
  	  //TODO
  	  break;
  case SYS_READ:/* Read from a file. */
  	  //TODO
  	  break;
  case SYS_WRITE:/* Write to a file or fd*/
	  {
		 int fd = ((int*)f->esp)[1];
		 const void *buf = (void*)((int*)f->esp)[2];
		 int size = ((int*)f->esp)[3];
		 if(fd == 1 /*&& is_valid_ptr(buf)*/) {
			 putbuf(buf, size);
			 f->eax = size;
		 }
		 else {
			 f->eax = -1;
		 }
		 break;
	  }
  	  break;
  case SYS_SEEK:/* Change position in a file. */
  	  //TODO
  	  break;
  case SYS_TELL:/* Report current position in a file. */
  	  //TODO
  	  break;
  case SYS_CLOSE:
  	  //TODO
  	  break;
  default:	break;
  }
}

void exit(int status){
	struct child_info *child;
	struct thread *t = thread_current();
	struct list_elem *e;
	struct thread *parent = get_thread_by_id(t->parent_id);

	enum intr_level old_level = intr_disable ();
	if(parent != NULL && !list_empty(&parent->children)) {
		for (e = list_begin (&parent->children); e != list_end (&parent->children); e = list_next (e)) {

			child = list_entry (e, struct child_info, child_elem);
			if(child->cid == t->tid) {
				lock_acquire(&parent->child_lock);
				child->has_exited = true;
				child->exit_status = status;
				lock_release(&parent->child_lock);
				//add new cond and signal
			}
		}
	}
	printf ("%s: exit(%d)\n", t->name, status);
	intr_set_level (old_level);
	thread_exit();
}

pid_t exec(const char *cmd_line) {
	//since there is one thread per process, pid = tid
	pid_t pid;
	tid_t tid;
	struct thread *t = thread_current();

	t->child_load_status = 0;
	tid = process_execute(cmd_line);
	lock_acquire(&t->child_lock);
	while(t->child_load_status == 0) {
		cond_wait(&t->child_cond, &t->child_lock);
	}

	if(t->child_load_status == -1) {
		tid = -1;
	}

	lock_release(&t->child_lock);
	//the one to one mapping
	pid = tid;
	return  pid;
}
